# ******Projet python jeu de rôle CDA 2021 *****


import random   

ENEMY_HEALTH = 50
PLAYER_HEALTH = 50
NUMBER_OF_POTIONS = 3
SKIP_TURN = False 
MENU_CHOIX = ["1", "2"]

while True:
     
    if SKIP_TURN:
        print("Vous passer votre tour")
                    
        SKIP_TURN = False
        
    else:
        user_choice = input("Choisissez entre attaque(1) ou utiliser une potion (2) ?")
        
        

        while user_choice not in MENU_CHOIX:
            print("Veuillez choisir une option valide")
        
            user_choice = input("Choisissez entre attaque (1) ou utiliser une potion (2) ? ")
        if user_choice == "1": 
            
            your_attack = random.randint(5, 10) 
            ENEMY_HEALTH -= your_attack
            print(f"Vous avez infligé {your_attack} points de dégats à l'ennemi")
        elif user_choice == "2" and NUMBER_OF_POTIONS > 0: 
            
            potion_health = random.randint(15, 50)
            PLAYER_HEALTH += potion_health
            NUMBER_OF_POTIONS -= 1
            SKIP_TURN = True
            print(f"Vous récupérez {potion_health} point de vie {NUMBER_OF_POTIONS} ? restantes")
        else:
            print("Vous n'avez plus de potions ....")
            continue
        if ENEMY_HEALTH <= 0:
            print("Le player a gagné le jeu")
            break
              
    enemy_attack = random.randint(5, 15) 
    PLAYER_HEALTH -= enemy_attack
    print(f"L'ennemi vous a infligé {enemy_attack} point de dégats")
    if PLAYER_HEALTH <= 0:
        print("Le player a perdu ")
        break
          
      
        
print(f"le reste des points de vie du player est {PLAYER_HEALTH} et celui de l'enemmi est {ENEMY_HEALTH}")
print("fin du jeu.")            
