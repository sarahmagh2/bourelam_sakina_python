#   Creer une classe pour représenter un livre


class  Livre:
    
    def __init__(self, nom, nombre_de_page, prix):
        self.nom = nom
        self.nombre_de_page = nombre_de_page
        self.prix = prix
    def afficher_nom(self):
        print(f"Le livre est {self.nom}")
    def afficher_nombre_de_page(self):
        print(f"Le nombre de page pour {self.nom} est {self.nombre_de_page}")
    def afficher_prix(self):
        print(f"Le prix pour {self.nom} est {self.prix}")

livre_HP = Livre("Harry Potter", "300", "10.99 €")
livre_LOTR = Livre("Le seigneur des Anneaux", "400", "13.99€")

livre_HP.afficher_nom()
livre_HP.afficher_nombre_de_page()
livre_HP.afficher_prix()
print("*********************************************")
livre_LOTR.afficher_nom()
livre_LOTR.afficher_nombre_de_page()
livre_LOTR.afficher_prix()





