# Creer une liste de course en utilisant le module JSON 


import json
import os
import sys
path1 = os.getcwd() + "/liste_course.json"
if os.path.exists(path1):
    with open(path1, "r") as f:

        liste = json.load(f)
else:        
    liste = []
menu = """Choisissez parmi les 5 options suivantes :
    1- Ajouter un élément à la liste
    2- Retirer un élément de la liste 
    3- Affichier la liste 
    4- Vider la liste 
    5- Quitter 
    ? Votre choix : """

liste_choix = ["1", "2", "3", "4", "5"]
while True:
    user_choice = input(menu)
    if user_choice not in liste_choix:
        print("Veuillez choisir une option valide ...")
        continue
    if user_choice == "1": 
        item = input("Entrez le nom d'un élément à ajouter à la liste de courses : ")
        liste.append(item)
        print(f"L'élément {item} a bien été ajouté à la liste.")
    elif user_choice == "2":
        item = input("Entrez le nom d'un élément à retirer de la liste de courses :")
        if item in liste:
            liste.remove(item)
            print(f"L'élément {item} a bien été supprimé de la liste.")
        else:
            print(f"L'élément {item} n'est pas dans la liste .")
    elif user_choice == "3":
        if liste:
            print("Voici le contenu de votre liste:")
            for i, item in enumerate(liste, 1):
                print(f"{i}. {item}")
        else:
            print("Votre liste ne contient aucun élément.")
    elif user_choice == "4":
        liste.clear()
        print("La liste a été vidé de son contenu")
    elif user_choice == "5":
        with open(path1, "w") as f:
            json.dump(liste, f)
            print(f)
        sys.exit()    

    


                







